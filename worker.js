var moment = require('moment');
const request = require('request');
const { Client } = require('camunda-external-task-client-js');
const logger = require('./logger/index');
const BearerTokenInterceptor = require('camunda-external-task-bearer-token-interceptor');
const { Variables } = require("camunda-external-task-client-js");

// ################ Keycloak ##########################
let bearerTokenInterceptor = null;
if (process.env.KEYCLOAK_CLIENT_ID && process.env.KEYCLOAK_CLIENT_SECRET && process.env.KEYCLOAK_BASE_URL && process.env.KEYCLOAK_REALM_ID) {
  bearerTokenInterceptor = new BearerTokenInterceptor({
    clientId: process.env.KEYCLOAK_CLIENT_ID,
    clientSecret: process.env.KEYCLOAK_CLIENT_SECRET,
    baseUrl: process.env.KEYCLOAK_BASE_URL,
    readlmId: process.env.KEYCLOAK_REALM_ID,
  });
}  

// ################ Camunda ##########################
const config = { baseUrl: process.env.CAMUNDA_URL, use: logger, interceptors: bearerTokenInterceptor, workerId: 'workflow-messages', interval: 1000 };
//const config = { baseUrl: 'http://localhost:8080/camunda/rest', use: logger };
const client = new Client(config);

var GoogleSpreadsheet = require('google-spreadsheet');
var doc = new GoogleSpreadsheet('1Nw2m2UYuHiHKnBWgIH7Z5HPfw2WQVnb2EDqD-6xmRCg');
var creds = require('./client_secret.json');
var async = require('async');
var sheet;

const axios = require('axios')
const smsUrl = 'http://212.124.121.186:9501/api?'
const smsUser = 'beintech'
const smsPass = 'RmB2l49F'
//const daysBefore = 7
//postgREST token:
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5hbHl0aWNzX3VzZXIifQ.Q8H_V6qh9w-mxHUY4_2IqmWNk_UdkbtacaOAsbP8peE'
const POSTGREST_TOKEN = process.env.POSTGREST_TOKEN?process.env.POSTGREST_TOKEN:token
const POSTGREST_URL = process.env.POSTGREST_URL
const KEYCLOAK_BASE_URL = process.env.KEYCLOAK_BASE_URL
const CAMUNDA_URL = process.env.CAMUNDA_URL
const KEYCLOAK_CLIENT_ID = process.env.KEYCLOAK_CLIENT_ID
const KEYCLOAK_CLIENT_SECRET = process.env.KEYCLOAK_CLIENT_SECRET
const KEYCLOAK_ADMIN_USER = process.env.KEYCLOAK_ADMIN_USER
const KEYCLOAK_ADMIN_PASSWORD = process.env.KEYCLOAK_ADMIN_PASSWORD
//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка: Заявка на организацию поездки. Забор данных с excel
client.subscribe('app-get-data', async function({ task, taskService }) {

  const emp = task.variables.getAllTyped().starter.value

  request.get({
    'url': POSTGREST_URL+'/k3_employee?user_id=eq.'+emp,
    'auth': {
      'bearer': POSTGREST_TOKEN
    }
  }, function(error, response, body){
    if(error){
      console.log('error:=' + error);
    } else {
      if(JSON.parse(body).length != 0){
        console.log('fio = ' + JSON.parse(body)[0].fio)
        const obj = JSON.parse(body)[0];
        const variables = new Variables().setAllTyped({
          fio: {
            value: obj.fio,
            type: 'String',
          },
          phone: {
            value: obj.phone,
            type: 'String',
          },
          position: {
            value: obj.position,
            type: 'String',
          },
          backToBack: {
            value: obj.backtoback,
            type: 'String',
          },
          originPlace: {
            value: obj.originplace,
            type: 'String',
          },
          hotel1: {
            value: obj.hotel1,
            type: 'String',
          },
          hotel2: {
            value: obj.hotel2,
            type: 'String',
          },
        })
        taskService.complete(task,variables);
      } else {
        const variables = new Variables().setAllTyped({
        })
        taskService.complete(task,variables);
      }
    }
  })
});

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка: Заявка на организацию поездки. Сохранение данных в excel
client.subscribe('app-save-data', async function({ task, taskService }) {

  const typedValues = task.variables.getAllTyped();
  var emp = typedValues.starter.value
  request({
      method: 'PATCH',
      uri: POSTGREST_URL+'/k3_employee?user_id=eq.'+emp,
      auth: {
        'bearer': POSTGREST_TOKEN
      },
      'content-type': 'application/json',
      body: JSON.stringify({
        mobilization_date: typedValues.mobilizationDate.value,
        demobilization_date: typedValues.demobilizationDate.value,
        departure_point1: typedValues.departurePoint1.value,
        camp1: typedValues.camp1.value,
        transport1: typedValues.transport1.value,
        from1: typedValues.from1.value,
        departure_date1: typedValues.departureDate1.value,
        hotel1: typedValues.hotel1.value,
        checkin_date1: typedValues.checkInDate1.value,
        checkout_date1: typedValues.checkOutDate1.value,
        departure_point2: typedValues.departurePoint2.value,
        camp2: typedValues.camp2.value,
        transport2: typedValues.transport2.value,
        from2: typedValues.from2.value,
        departure_date2: typedValues.departureDate2.value,
        hotel2: typedValues.hotel2.value,
        checkin_date2: typedValues.checkInDate2.value,
        checkout_date2: typedValues.checkOutDate2.value
      })
    }, function(error, response, body){
      if(error){
        console.log('error:=' + error);
      } else {
        console.log(body)
      }
  })

  await taskService.complete(task);
});

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка: Массовое согласование. Забор данных с excel при мобилизации
client.subscribe('mass-mobile-get-data', function({ task, taskService }) {
  const typedValues = task.variables.getAllTyped();
  var daysBefore = parseInt(typedValues.daysBefore.value)
  daysBefore = daysBefore != null ? daysBefore : 5
  var shiftDate = moment(new Date()).add(daysBefore, 'days').format('YYYY-MM-DD');
  console.log('shiftDate = ' + shiftDate)
        var list = [];
        var isRows = 'NO'

  request.get({
    'url': POSTGREST_URL+'/k3_employee?mobilization_date=eq.'+shiftDate,
    'auth': {
      'bearer': POSTGREST_TOKEN
    }
  }, function(error, response, body){
    if(error){
      console.log('error:=' + error);
    } else {
      if(JSON.parse(body).length != 0){
        console.log('fio = ' + JSON.parse(body)[0].fio)
        const objects = JSON.parse(body);
        objects.forEach(element => {
          list.push({
            startDate: element.mobilization_date,
            endDate: element.demobilization_date,
            fio: element.fio,
            userId: element.user_id,
            phone: element.phone,
            email: element.email,
            position: element.position,
            backToBack: element.back_to_back,
            originPlace: element.origin_place,
            coordinatorPhone: element.coordinator_phone,
          });     

          isRows = 'YES'
        });
      
        const variables = new Variables().setAllTyped({
          employees: {
            value: list,
            type: 'Json'
          },
          shiftDate: {
            value: moment(new Date()).add(daysBefore, 'days').format('DD.MM.YYYY'),
            type: 'String'
          },   
          isRows: {
            value: isRows,
            type: 'String'
          }   
        });

        taskService.complete(task,variables);
      } else {
        const variables = new Variables().setAllTyped({
          employees: {
            value: list,
            type: 'Json'
          },
          shiftDate: {
            value: moment(new Date()).add(daysBefore, 'days').format('DD.MM.YYYY'),
            type: 'String'
          },   
          isRows: {
            value: isRows,
            type: 'String'
          }   
        })
        taskService.complete(task,variables);
      }
    }
  })
});

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка: Массовое согласование. Забор данных с excel при демобилизации
client.subscribe('mass-demobile-get-data', function({ task, taskService }) {
  const typedValues = task.variables.getAllTyped();
  var daysBefore = parseInt(typedValues.daysBefore.value)
  daysBefore = daysBefore != null ? daysBefore : 5
  var shiftDate = moment(new Date()).add(daysBefore, 'days').format('YYYY-MM-DD');
  console.log('shiftDate = ' + shiftDate)
        var list = [];
        var isRows = 'NO'

  request.get({
    'url': POSTGREST_URL+'/k3_employee?demobilization_date=eq.'+shiftDate,
    'auth': {
      'bearer': POSTGREST_TOKEN
    }
  }, function(error, response, body){
    if(error){
      console.log('error:=' + error);
    } else {
      if(JSON.parse(body).length != 0){
        console.log('fio = ' + JSON.parse(body)[0].fio)
        const objects = JSON.parse(body);
        objects.forEach(element => {
          list.push({
            startDate: element.mobilization_date,
            endDate: element.demobilization_date,
            fio: element.fio,
            userId: element.user_id,
            phone: element.phone,
            email: element.email,
            position: element.position,
            backToBack: element.back_to_back,
            originPlace: element.origin_place,
            coordinatorPhone: element.coordinator_phone,
          });     

          isRows = 'YES'
        });
      
        const variables = new Variables().setAllTyped({
          employees: {
            value: list,
            type: 'Json'
          },
          shiftDate: {
            value: moment(new Date()).add(daysBefore, 'days').format('DD.MM.YYYY'),
            type: 'String'
          },   
          isRows: {
            value: isRows,
            type: 'String'
          }   
        });

        taskService.complete(task,variables);
      } else {
        const variables = new Variables().setAllTyped({
          employees: {
            value: list,
            type: 'Json'
          },
          shiftDate: {
            value: moment(new Date()).add(daysBefore, 'days').format('DD.MM.YYYY'),
            type: 'String'
          },   
          isRows: {
            value: isRows,
            type: 'String'
          }   
        });
        taskService.complete(task,variables);
      }
    }
  })

});

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка: Массовое согласование. Сохранение данных в excel при мобилизации
client.subscribe('mass-mobile-save-data', async function({ task, taskService }) {

  const emp = task.variables.getAllTyped().employee.value;

  request({
      method: 'PATCH',
      uri: POSTGREST_URL+'/k3_employee?user_id=eq.'+emp.userId,
      auth: {
        'bearer': POSTGREST_TOKEN
      },
      'content-type': 'application/json',
      body: JSON.stringify({
        mobilization_date: emp.newStartDate,
        demobilization_date: emp.newEndDate,
      })
    }, function(error, response, body){
      if(error){
        console.log('error:=' + error);
      } else {
        console.log(body)
      }
  })

  await taskService.complete(task);
});

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка: Массовое согласование. Сохранение данных в excel при демобилизации
client.subscribe('mass-demobile-save-data', async function({ task, taskService }) {

  const emp = task.variables.getAllTyped().employee.value;

  request({
      method: 'PATCH',
      uri: POSTGREST_URL+'/k3_employee?user_id=eq.'+emp.userId,
      auth: {
        'bearer': POSTGREST_TOKEN
      },
      'content-type': 'application/json',
      body: JSON.stringify({
        demobilization_date: emp.newEndDate,
      })
    }, function(error, response, body){
      if(error){
        console.log('error:=' + error);
      } else {
        console.log(body)
      }
  })

  await taskService.complete(task);
});

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка: Массовое согласование. Проверка на наличие записей при мобилизации
client.subscribe('mass-mobile-check-data', function({ task, taskService }) {
  const typedValues = task.variables.getAllTyped();
  const date = typedValues.shiftDate.value.trim();
  var shiftDate = moment(new Date(date.substring(3,5) + '.' + date.substring(0,2) + '.' + date.substring(6,10))).format('YYYY-MM-DD');
  
  request.get({
    'url': POSTGREST_URL+'/k3_employee?mobilization_date=eq.'+shiftDate,
    'auth': {
      'bearer': POSTGREST_TOKEN
    }
  }, function(error, response, body){
    if(error){
      console.log('error:=' + error);
    } else {
        const objects = JSON.parse(body);
        var isRows = 'NO'
        if(objects.length != 0){
          isRows = 'YES'
        }
        const variables = new Variables().setAllTyped({
          isRows: {
            value: isRows,
            type: 'String'
          }   
        });

        taskService.complete(task,variables);
    }
  })
});

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка: Массовое согласование. Проверка на наличие записей при демобилизации
client.subscribe('mass-demobile-check-data', function({ task, taskService }) {
  const typedValues = task.variables.getAllTyped();
  const date = typedValues.shiftDate.value.trim();
  var shiftDate = moment(new Date(date.substring(3,5) + '.' + date.substring(0,2) + '.' + date.substring(6,10))).format('YYYY-MM-DD');
  
  request.get({
    'url': POSTGREST_URL+'/k3_employee?demobilization_date=eq.'+shiftDate,
    'auth': {
      'bearer': POSTGREST_TOKEN
    }
  }, function(error, response, body){
    if(error){
      console.log('error:=' + error);
    } else {
        const objects = JSON.parse(body);
        var isRows = 'NO'
        if(objects.length != 0){
          isRows = 'YES'
        }
        const variables = new Variables().setAllTyped({
          isRows: {
            value: isRows,
            type: 'String'
          }   
        });

        taskService.complete(task,variables);
    }
  })

});

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка: Массовое согласование. Отправка СМС сотрудникам при мобилизации
client.subscribe('mass-mobile-send-sms', function({ task, taskService }) {

  const typedValues = task.variables.getAllTyped();
  const date = typedValues.shiftDate.value.trim();
  var shiftDate = moment(new Date(date.substring(3,5) + '.' + date.substring(0,2) + '.' + date.substring(6,10))).format('YYYY-MM-DD');

  request.get({
    'url': POSTGREST_URL+'/k3_employee?mobilization_date=eq.'+shiftDate,
    'auth': {
      'bearer': POSTGREST_TOKEN
    }
  }, function(error, response, body){
    if(error){
      console.log('error:=' + error);
    } else {
      if(JSON.parse(body).length != 0){
        const objects = JSON.parse(body);
        objects.forEach(element => {
          var sms_text = `Camp: ${element.camp1}. ${moment(typedValues.departureDate.value).format('DD/MM/YYYY')} otpravlenie avtobusa na Prorvu s aeroporta v 08:15; s ofisa Kazpaco na Satpaeva 23b v 09:15; s zh.d.vokzala v 10:00; s Dossor v 11:40, s Kulsary v 14:20(kafe Ak Zhol). ${typedValues.transport.value}. Voditel' ${typedValues.driver.value}.`
          axios.get(smsUrl, {
            params: {
              action: 'sendmessage',
              username: smsUser,
              password: smsPass,
              recipient: '7'+element.phone,
              messagetype: 'SMS:TEXT',
              originator: 'BI',
              messagedata: sms_text,
            }
          })
          .catch(function (error) {
            console.log(error);
          })
        });

        taskService.complete(task);
      } else {
        taskService.complete(task);
      }
    }
  })
});

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка: Массовое согласование. Отправка СМС сотрудникам при демобилизации
client.subscribe('mass-demobile-send-sms', function({ task, taskService }) {

  const typedValues = task.variables.getAllTyped();
  const date = typedValues.shiftDate.value.trim();
  var shiftDate = moment(new Date(date.substring(3,5) + '.' + date.substring(0,2) + '.' + date.substring(6,10))).format('YYYY-MM-DD');

  request.get({
    'url': POSTGREST_URL+'/k3_employee?demobilization_date=eq.'+shiftDate,
    'auth': {
      'bearer': POSTGREST_TOKEN
    }
  }, function(error, response, body){
    if(error){
      console.log('error:=' + error);
    } else {
      if(JSON.parse(body).length != 0){
        const objects = JSON.parse(body);
        objects.forEach(element => {
          var sms_text = `Camp: ${element.camp2}. ${moment(typedValues.departureDate.value).format('DD/MM/YYYY')} otpravlenie avtobusa v Atyrau iz kempa Unexstroi v 08:00; iz BI kempa 09:00. ${typedValues.transport.value}. Voditel' ${typedValues.driver.value}.`
          axios.get(smsUrl, {
            params: {
              action: 'sendmessage',
              username: smsUser,
              password: smsPass,
              recipient: '7'+element.phone,
              messagetype: 'SMS:TEXT',
              originator: 'BI',
              messagedata: sms_text,
            }
          })
          .catch(function (error) {
            console.log(error);
          })

          request({
              method: 'PATCH',
              uri: POSTGREST_URL+'/k3_employee?user_id=eq.'+element.user_id,
              auth: {
                'bearer': POSTGREST_TOKEN
              },
              'content-type': 'application/json',
              body: JSON.stringify({
                mobilization_date: moment(new Date()).add(29, 'days').format('YYYY-MM-DD'),
                demobilization_date: moment(new Date()).add(57, 'days').format('YYYY-MM-DD'),
              })
            }, function(error, response, body){
              if(error){
                console.log('error:=' + error);
              } else {
                console.log(body)
              }
          })

        });

        taskService.complete(task);
      } else {
        taskService.complete(task);
      }
    }
  })
});

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка: Массовое создание камунда и киклок пользователей

client.subscribe('camunda', function({ task, taskService }) {
  //0. пользователи которых нет в камунда
  request.get({
    'url': POSTGREST_URL+'/k3_employee?camunda=eq.false',
    'auth': {
      'bearer': POSTGREST_TOKEN
    }
  }, function(error0, response0, body0){
    if(error0){
      console.log('error:=' + error0);
    } else if(body0){
      let newEmployees = JSON.parse(body0);
      if(newEmployees.length != 0)
        camundaIterate(0, taskService, task, newEmployees);
      else
        taskService.complete(task);
    }
  });
});

client.subscribe('keycloak', function({ task, taskService }) {
  //3. пользователи которых нет в киклок
  request.get({
    'url': POSTGREST_URL+'/k3_employee?keycloak=eq.false',
    'auth': {
      'bearer': POSTGREST_TOKEN
    }
  }, function(error1, response1, body1){
    if(error1){
      console.log(error1)
    } else if(body1){
      let employees = JSON.parse(body1)
      if(employees.length != 0)
        keycloakIterate(0, taskService, task, employees);
      else
        taskService.complete(task);        
    }
  });
});

function camundaIterate(index, taskService, task, newEmployees){
      var element = newEmployees[index]
      if(element){
        const user = {
          "profile": {
            "id": element.user_id,
            "firstName": element.fio.split(" ")[0],
            "lastName": element.fio.split(" ")[1]?element.fio.split(" ")[1]:'',
            "email": element.email
          },
          "credentials": {
            "password": element.user_id
          }
        }
        //console.log(user)
        //1. авторизация для камунды
        request({
          method: 'POST',
          uri: KEYCLOAK_BASE_URL+'/auth/realms/bi-group/protocol/openid-connect/token',
          auth: {
            'username': KEYCLOAK_CLIENT_ID,
            'password': KEYCLOAK_CLIENT_SECRET
          },
          'content-type': 'application/x-www-form-urlencoded',
          form: {
            grant_type: 'client_credentials'
          }
        }, function(error1, response1, body1){
          if(error1){
            console.log('camunda auth error:=' + error1);
          } else if(response1.statusCode != 200) {
            console.log('camunda auth error:=' + body1.error + ' ' + body1.error_description)
          } else if(body1){
            const resp = JSON.parse(body1)
            let camunda_access_token = resp.access_token
            //2. создать пользователя и добавить в группу BMEI_EMPLOYEE
            createCamundaUser(user, camunda_access_token, element.user_id)
            index = index+1;
            if(newEmployees.length != index)
              camundaIterate(index, taskService, task, newEmployees)
            else
            {
              taskService.complete(task);
            }
          }
        });
      }
}
function createCamundaUser(user, access_token, user_id){
  request({
    method: 'GET',
    uri: CAMUNDA_URL + '/user/'+user_id+'/profile',
    auth: {
      bearer: access_token
    }
  }, function(error4, response4, body4){
    if(error4){
      console.log(error4)
    }
    if(response4.statusCode == 404){
      request({
        method: 'POST',
        uri: CAMUNDA_URL + '/user/create',
        auth:{
          bearer: access_token
        },
        headers:{
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
      }, function(error2, response2, body2){
        if(error2){
          console.log('user_id:=' + user_id + ' createCamundaUser error:'+error2)
        } else if(response2.statusCode != 401){
          request({
            method: 'PUT',
            uri: CAMUNDA_URL + '/group/BMEI_EMPLOYEE/members/'+user_id,
            auth:{
              bearer: access_token
            }
          }, function(error, response, body){
            if(error){
              console.log('user_id:=' + user_id + ' createCamundaUserAddGroup BMEI_EMPLOYEE error:'+error)
            } else {
              request({
                method: 'PUT',
                uri: CAMUNDA_URL + '/group/USER/members/'+user_id,
                auth:{
                  bearer: access_token
                }
              }, function(error, response, body){
                if(error){
                  console.log('user_id:=' + user_id + ' createCamundaUserAddGroup USER error:'+error)
                } else {
                  request({
                    method: 'PATCH',
                    uri: POSTGREST_URL + '/k3_employee?user_id=eq.'+user_id,
                    auth:{
                      'bearer': POSTGREST_TOKEN
                    },
                    'content-type': 'application/json',
                    body: JSON.stringify({
                      camunda: true
                    })
                  }, function(err, res, b){
                    if(err){
                      console.log('camundaError user_id:=' + user_id + ' '+err)
                    } else {
                      console.log('camundaSuccess user_id:=' + user_id + ' '+res.statusCode)
                    }
                  });
                }
              })
            }
          })
        } else {
          console.log(response2.code)
        }
      })
    } else {
      request({
        method: 'PATCH',
        uri: POSTGREST_URL + '/k3_employee?user_id=eq.'+user_id,
        auth:{
          'bearer': POSTGREST_TOKEN
        },
        'content-type': 'application/json',
        body: JSON.stringify({
          camunda: true
        })
      }, function(err, res, b){
        if(err){
          console.log('camundaError user_id:=' + user_id + ' '+err)
        } else {
          console.log('camundaSuccess user_id:=' + user_id + ' '+res.statusCode)
        }
      });
    }
  })
}

function keycloakIterate(index, taskService, task, employees){
      var element = employees[index]
      if(element){
        const userK = {
          "username": element.user_id,
          "enabled": true,
          "firstName": element.fio.split(" ")[0],
          "lastName": element.fio.split(" ")[1]?element.fio.split(" ")[1]:'',
          "email": element.email,
          "credentials": [{
            "type": "password",
            "value": element.user_id
          }]
        }
        //console.log(userK)
        //4. авторизация для киклок
        request({
          method: 'POST',
          uri: KEYCLOAK_BASE_URL + '/auth/realms/master/protocol/openid-connect/token',
          'content-type': 'application/x-www-form-urlencoded',
          form: {
            client_id: 'admin-cli',
            username: KEYCLOAK_ADMIN_USER,
            password: KEYCLOAK_ADMIN_PASSWORD,
            grant_type: 'password'
          }
        }, function(error, response, body){
          if(error){
            console.log('keycloak auth error='+error)
          } else if (response.statusCode != 200) {
            console.log('keycloak auth error=' + body.error + ' ' + body.error_description)
          } else if(body){
            const resp = JSON.parse(body)
            let keycloak_access_token = resp.access_token
            //5. создать пользователя
            createKeycloakUser(userK, keycloak_access_token, element.user_id)
            index = index+1;
            if(employees.length != index)
              keycloakIterate(index, taskService, task, employees)
            else
            {
              taskService.complete(task);
            }
          }
        });
      }
}
function createKeycloakUser(user, keycloak_token, user_id){
  request({
    method: 'GET',
    uri: KEYCLOAK_BASE_URL + '/auth/admin/realms/bi-group/users?username=' + user_id,
    auth: {
      bearer: keycloak_token
    },
  }, function(error5, response5, body5){
    if(error5){
      console.log(error5)
    } else if(response5.statusCode == 200 && JSON.parse(body5).length == 0){
      request({
        method: 'POST',
        uri: KEYCLOAK_BASE_URL + '/auth/admin/realms/bi-group/users',
        auth: {
          bearer: keycloak_token
        },
        headers: {
          'Content-Type': 'application/json', 
        },
        body: JSON.stringify(user)   
      }, function(error2, response2, body2){
        if(error2){
          console.log('user_id:='+user_id+' createKeycloakUser='+error2)
        } 
        if(response2.statusCode == 201){
          request({
            method: 'PATCH',
            uri: POSTGREST_URL + '/k3_employee?user_id=eq.'+user_id,
            auth:{
              'bearer': POSTGREST_TOKEN
            },
            'content-type': 'application/json',
            body: JSON.stringify({
              keycloak: true
            })
          }, function(err, res, b){
            if(err){
              console.log('keycloakError user_id:='+user_id+' '+err)
            } else {
              console.log('keycloakSuccess user_id:='+user_id+' '+res.statusCode)
            }
          })
        } else {
          console.log(body2)
        }
      })
    }
  });
}

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка-Табельный: Забор данных с базы
client.subscribe('rest-get-list', async function({ task, taskService }) {

  request.get({
    'url': POSTGREST_URL+'/k3_employee',
    'auth': {
      'bearer': POSTGREST_TOKEN
    }
  }, function(error, response, body){
    if(error){
      console.log('error:=' + error);
    } else {
      if(body){
        const objects = JSON.parse(body);
        const variables = new Variables().setAllTyped({
          employees: {
            value: objects,
            type: 'Json',
          },
        })
        taskService.complete(task,variables);
      }
    }
  })
});

//---------------------------------------------------------------------------------------------------------------------------
// Перевахтовка-Табельный: Забор данных с базы

client.subscribe('add-edit-employee', async function({ task, taskService }) {
  let employeesEdited = task.variables.getAllTyped().employeesEdited.value;
  console.log('employeesEdited.length='+employeesEdited.length)
  if(employeesEdited.length != 0){
      addEditEmployeeIterate(0, task, taskService, employeesEdited)
  } else {
    console.log('cancel')
    taskService.complete(task);
  }
});

function addEditEmployeeIterate(index, task, taskService, employeesEdited){
  var element = employeesEdited[index]
  element.tab_no = element.user_id
  const status = element.status
  const user = {
    user_id: element.user_id,
    tab_no: element.tab_no,
    fio: element.fio,
    fio_ru: element.fio_ru,
    phone: element.phone,
    email: element.email,
    position: element.position,
    back_to_back: element.back_to_back,
    origin_place: element.origin_place,
    coordinator_phone: element.coordinator_phone,
    mobilization_date: element.mobilization_date,
    demobilization_date: element.demobilization_date,
    hotel1: element.hotel1,
    hotel2: element.hotel2,
    camp1: element.camp1,
    camp2: element.camp2,
    deleted: element.deleted,
    direct: element.direct
  }
  if(status == 'Изменен'){
    request({
      method: 'PATCH',
      uri: POSTGREST_URL+'/k3_employee?user_id=eq.'+element.user_id,
      'auth': {
        'bearer': POSTGREST_TOKEN
      },
      'content-type': 'application/json',
      body: JSON.stringify(user)
    }, function(error, response, body){
      if(error){
        console.log('error:=' + error);
      } else {
        const variables = new Variables().setAllTyped({
        })
        index++
        if(index != employeesEdited.length)
          addEditEmployeeIterate(index, task, taskService, employeesEdited)
        else
          taskService.complete(task,variables);
      }
    })
  } else if (status == "Новый") {
    request({
      method: 'POST',
      uri: POSTGREST_URL+'/k3_employee',
      'auth': {
        'bearer': POSTGREST_TOKEN
      },
      'content-type': 'application/json',
      body: JSON.stringify(user)
    }, function(error, response, body){
      if(error){
        console.log('error:=' + error);
      } else {
        const variables = new Variables().setAllTyped({
        })
        index++
        if(index != employeesEdited.length)
          addEditEmployeeIterate(index, task, taskService, employeesEdited)
        else
          taskService.complete(task,variables);
      }
    })
  } else {
    request({
      method: 'DELETE',
      uri: POSTGREST_URL + '/k3_employee?user_id=eq.' + element.user_id,
      'auth': {
        'bearer': POSTGREST_TOKEN
      },
      'content-type': 'application/json',
      body: JSON.stringify(user)
    }, function(error, response, body){
      if(error){
        console.log('error:=' + error);
      } else if(response.statusCode == 204){
        camundaDeleteUser(index, task, taskService, employeesEdited)
      } else {
        console.log('user_id='+element.user_id + ' postgrestDelete message:=' + body)
      }
    })
  }
}

function camundaDeleteUser(index, task, taskService, employeesEdited) {
  var element = employeesEdited[index]
  //1. авторизация для камунды
  request({
    method: 'POST',
    uri: KEYCLOAK_BASE_URL+'/auth/realms/bi-group/protocol/openid-connect/token',
    auth: {
      'username': KEYCLOAK_CLIENT_ID,
      'password': KEYCLOAK_CLIENT_SECRET
    },
    'content-type': 'application/x-www-form-urlencoded',
    form: {
      grant_type: 'client_credentials'
    }
  }, function(error1, response1, body1){
    if(error1){
      console.log('camunda auth error:=' + error1);
    } else if (response1.statusCode != 200) {
      console.log('camunda auth error:=' + body1.error + ' ' + body1.error_description)
    } else if(body1){
      const resp = JSON.parse(body1)
      let camunda_access_token_ = resp.access_token
      request({
        method: 'DELETE',
        uri: CAMUNDA_URL + '/user/' + element.user_id,
        'auth': {
          bearer: camunda_access_token_
        },
      }, function(error2, response2, body2){
        if(error2){
          console.log('error:=' + error2);
        } else if(response2.statusCode == 204){
          keycloakDeleteUser(index, task, taskService, employeesEdited)
        } else {
          console.log('user_id='+element.user_id+' camundaDeleteUser message:=' + body2)
        }
      })
    }
  });
}

function keycloakDeleteUser(index, task, taskService, employeesEdited) {
  var element = employeesEdited[index]
  //4. авторизация для киклок
  request({
    method: 'POST',
    uri: KEYCLOAK_BASE_URL + '/auth/realms/master/protocol/openid-connect/token',
    'content-type': 'application/x-www-form-urlencoded',
    form: {
      client_id: 'admin-cli',
      username: KEYCLOAK_ADMIN_USER,
      password: KEYCLOAK_ADMIN_PASSWORD,
      grant_type: 'password'
    }
  }, function(error, response, body){
    if(error){
      console.log('keycloak auth error =' + error)
    } else if(response.statusCode != 200) {
      console.log('keycloak auth error =' + body.error + ' ' + body.error_description)
    } else if(body){
      const resp = JSON.parse(body)
      var keycloak_access_token_ = resp.access_token
      //5. создать пользователя
      request({
        method: 'GET',
        uri: KEYCLOAK_BASE_URL + '/auth/admin/realms/bi-group/users?username='+element.user_id,
        auth: {
          bearer: keycloak_access_token_
        },
      }, function(error2, response2, body2){
        if(error2){
          console.log(error2)
        } else if (response2.statusCode == 200 && JSON.parse(body2).length !=0) {
          let user = JSON.parse(body2)[0]
          request({
            method: 'DELETE',
            uri: KEYCLOAK_BASE_URL + '/auth/admin/realms/bi-group/users/'+user.id,
            auth: {
              bearer: keycloak_access_token_
            },
          }, function(error3, response3, body3){
            if (error3) {
              console.log(error3)
            } else if (response3.statusCode == 204) {
              index++
              if(index != employeesEdited.length)
                addEditEmployeeIterate(index, task, taskService, employeesEdited)
              else
                taskService.complete(task);
            } else {
              console.log('user_id='+element.user_id+' keycloakDeleteUser message:=' +body3)
            }
          });
        }
      })
    }
  });
}